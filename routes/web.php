<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Route::any('{all}', function(){
//     if(Auth::check()){
//     return view('admin.login');
// }
// });

        //Route  Menu
Route::middleware(['admin'])->group(function () {
    Route::get('menu', 'MenuController@showtable')->name('menu');
    Route::post('insert_menu', 'MenuController@insert')->name('insert_menu');
    Route::post('delete-menu', 'MenuController@delete')->name('delete-menu');
    Route::post('update-menu', 'MenuController@update')->name('update-menu');
    Route::get('admin', function () {
        return view('layouts.master_admin');
    })->name('admin');
    //Route Home Page
    Route::get('homepage', 'HomePageController@showtable')->name('homepage');
    Route::post('insert_homepage', 'HomePageController@insert')->name('insert_homepage');
    Route::post('delete-homepage', 'HomePageController@delete')->name('delete-homepage');
    Route::post('update-homepage', 'HomePageController@update')->name('update-homepage');
    //Route About-us Homepage
    Route::get('about_us', 'AboutUsController@showtable')->name('about_us');
    Route::post('insert_about_us', 'AboutUsController@insert')->name('insert_about_us');
    Route::post('delete-about_us', 'AboutUsController@delete')->name('delete-about_us');
    Route::post('update-about_us', 'AboutUsController@update')->name('update-about_us');
    //Route OutProcess Homepage
    Route::get('our_process', 'OutProcessController@showtable')->name('out_process');
    Route::post('insert_out-process', 'OutProcessController@insert')->name('insert_out-process');
    Route::post('delete-out-process', 'OutProcessController@delete')->name('delete-out-process');
    Route::post('update-out-process', 'OutProcessController@update')->name('update-out-process');
    //Route Out_client  ourprocess Homepage
    Route::get('our_client', 'OutClientController@showtable')->name('our_client');
    Route::post('insert_our_client', 'OutClientController@insert')->name('insert_our_client');
    Route::post('delete-our_client', 'OutClientController@delete')->name('delete-our_client');
    Route::post('update-our_client', 'OutClientController@update')->name('update-our_client');
    //Route OurWorks Homepage
    Route::get('our_works', 'OurWorksController@showtable')->name('our_works');
    Route::post('insert_our-works', 'OurWorksController@insert')->name('insert_our-works');
    Route::post('delete-our-works', 'OurWorksController@delete')->name('delete-our-works');
    Route::post('update-our-works', 'OurWorksController@update')->name('update-our-works');
    //Route element menu Ourworks Homepage
    Route::get('element_ourworks', 'MenuOurWorksController@showtable')->name('element_ourworks');
    Route::post('insert_element-menu', 'MenuOurWorksController@insert')->name('insert_element-menu');
    Route::post('delete-element-menu', 'MenuOurWorksController@delete')->name('delete-element-menu');
    Route::post('update-element-menu', 'MenuOurWorksController@update')->name('update-element-menu');
    //Route OurCustomers Homepage
    Route::get('our_customers', 'OurCustomersController@showtable')->name('our_customers');
    Route::post('insert_our-customers', 'OurCustomersController@insert')->name('insert_our-customers');
    Route::post('delete-our-customers', 'OurCustomersController@delete')->name('delete-our-customers');
    Route::post('update-our-customers', 'OurCustomersController@update')->name('update-our-customers');
    //Route OurTeam Homepage
    Route::get('our_team', 'OurTeamController@showtable')->name('our_team');
    Route::post('insert_our-team', 'OurTeamController@insert')->name('insert_our-team');
    Route::post('delete-our-team', 'OurTeamController@delete')->name('delete-our-team');
    Route::post('update-our-team', 'OurTeamController@update')->name('update-our-team');
    //About Us Page - lane1
     //Route OurTeam Homepage
    Route::get('about-us-lane1', 'Lane1Controller@showtable')->name('about-us-lane1');
    Route::post('insert-lane1', 'Lane1Controller@insert')->name('insert-lane1');
    Route::post('delete-lane1', 'Lane1Controller@delete')->name('delete-lane1');
    Route::post('update-lane1', 'Lane1Controller@update')->name('update-lane1');
    //services
    Route::get('services', 'ServicesController@showtable')->name('services');
    Route::post('insert-services', 'ServicesController@insert')->name('insert-services');
    Route::post('delete-services', 'ServicesController@delete')->name('delete-services');
    Route::post('update-services', 'ServicesController@update')->name('update-services');
});
// //ruote quan ly dang nhap
Route::get('showlogin', 'AuthenticationController@showLogin')->name('showLogin');
Route::get('showregister', 'AuthenticationController@showRegister')->name('showRegister');
Route::post('login', 'AuthenticationController@Login')->name('Login');
Route::post('register', 'AuthenticationController@Register')->name('Register');
Route::get('logout', 'AuthenticationController@LogOut')->name('logout');

Route::get('home_page', 'PageController@HomePage')->name('home_page');
Route::get('about_us_page', 'PageController@AboutUsPage')->name('about_us_page');
Route::get('services_page', 'PageController@ServicesPage')->name('services_page');
Route::get('contact_me_page', 'PageController@ContactMePage')->name('contact_me_page');

Route::post('sendmail', 'PageController@SendMail')->name('sendmail');

Route::get('test', function () {
    $test = App\ElementMenuOurWorks::all();
    echo "<pre>";
    echo ($test[0]->image);
    echo "</pre>";
});