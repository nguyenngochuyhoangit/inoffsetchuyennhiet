$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-element-menu",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "element_ourworks";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var name_img = $("#name_img" + id).html();
        var link_img = $("#link_img" + id).html();
        var link_event = $("#link_event" + id).html();
        $('#update_modal').val(id);
        $('#name_img_modal').val(name_img);
        $('#link_img_modal').val(link_img);
        $('#link_event_modal').val(link_event);
    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var name_img = $('#name_img_modal').val();
        var link_img = $('#link_img_modal').val();
        var link_event = $('#link_event_modal').val();
        var area = $('#forign_key_modal').find(":selected").val();
        var data = {
            'id': id,
            'name_img': name_img,
            'link_img': link_img,
            'link_event':link_event,
            'area':area
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-element-menu",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#name_img' + id).html(data.name_image);
                $('#link_img' + id).html(data.link_image);
                $('#link_event' + id).html(data.link_event);
                $('#area' + id).html(data.menu_id);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
