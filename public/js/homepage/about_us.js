$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-about_us",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "about_us";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var title = $("#tite" + id).html();
        var description = $("#description" + id).html();
        var alt_img = $("#alt_img" + id).html();
        $('#update_modal').val(id);
        $('#title').val(title);
        $('#descrtption').val(description);
        $('#alt_img').val(alt_img);
     

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var title = $('#title_modal').val();
        var description = $('#description_modal').val();
        var image = $('#image_modal').val();
        var alt_img = $('#alt_img_modal').val();
        var data = {
            'id': id,
            'title': title,
            'description': description,
            'image': image,
            'alt_img': alt_img
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-about_us",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#title' + id).html(data.title);
                $('#description' + id).html(data.descrtiption);
                $('#image' + id).html(data.image);
                $('#alt_img' + id).html(data.alt_img);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
