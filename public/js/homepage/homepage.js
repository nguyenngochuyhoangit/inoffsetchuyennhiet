$(function () {
    $('#example1 .btn_delete').click(function () {
        var id = $(this).val();
        var csrf = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "delete-homepage",
            type: "POST",
            data: {
                'id': id
            },
            contentType: "application/x-www-form-urlencoded",
            success: function (data) {
                window.location = "homepage";

            }
        })
    })

    /*update*/  
    $('#example1 tr td  #modal').click(function () {
        var id = $(this).val();
        var first_title = $("#first_title" + id).html();
        var last_title = $("#last_title" + id).html();
        $('#update_modal').val(id);
        $('#first_title_modal').val(first_title);
        $('#last_title_modal').val(last_title);
     

    });
    $('.modal-footer  #update_modal').click(function () {
        var id = $(this).val();
        var first_title = $('#first_title_modal').val();
        var last_title = $('#last_title_modal').val();
        var data = {
            'id': id,
            'first_title': first_title,
            'last_title': last_title
        };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "update-homepage",
            type: "POST",
            data: data,
            //contentType: "application/json",
            dataType: 'json',
            success: function (data) { //sau done, success trả về return của url
                $('#first_title' + id).html(data.first_title);
                $('#last_title' + id).html(data.last_title);
                $('#modal-info').modal('hide');//update va2 close modal
             
            }
        })
    });
})
