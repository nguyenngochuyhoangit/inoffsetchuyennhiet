<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class our_works extends Model
{
    protected $table ='our_works_menu';
    protected $fillable = [
        'menu'
    ];
    public $timestamps = false;

    public function elementMenu()
    {
        return $this->hasMany('App\ElementMenuOurWorks', 'id');
    }
}
