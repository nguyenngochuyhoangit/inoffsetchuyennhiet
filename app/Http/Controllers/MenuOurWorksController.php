<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ElementMenuOurWorks;
use App\our_works;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
class MenuOurWorksController extends Controller
{
    public function showtable()
    {
        $data_ourworks = our_works::all();
        $data = ElementMenuOurWorks::all();
        return view('element_ourworks', ['data' => $data,'data_ourworks' => $data_ourworks]);
    }

    public function insert(Request $request)
    {
        // insert db
        $filename = $_FILES['image']['name'];
        Storage::putFileAs('public',new File($_FILES['image']['tmp_name']),$filename);
        $url = Storage::url( $filename);
        $insert = new ElementMenuOurWorks;
        $insert->menu_id="$request->forign_key";
        $insert->image ="$url";
        $insert->name_image="$request->name_img";
        $insert->link_image="$request->link_img";
        $insert->link_event="$request->link_event";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        ElementMenuOurWorks::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $name_img=$_POST["name_img"];
        $link_img= $_POST["link_img"];
        $link_event= $_POST["link_event"];
        $area= $_POST["area"];
        //update db homepage
        ElementMenuOurWorks::where('id',$ids)->update([
            'name_image'=>$name_img,
            'link_image'=>$link_img,
            'link_event'=>$link_event,
            'menu_id'=>$area,
        ]);
        $data = ElementMenuOurWorks::find($ids);
        return $data;
    }
}
