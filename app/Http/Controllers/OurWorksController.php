<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\our_works;
class OurWorksController extends Controller
{
    public function showtable()
    {
        $data = our_works::all();
        return view('our_works', ['data' => $data]);
    }

    public function insert(Request $request)
    {
        // insert db
        $insert = new our_works;
        $insert->menu ="$request->menu_title";
        $insert->save();
        return redirect()->back();
       
    }
    public function delete()
    {
        our_works::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $menu_title =$_POST["menu_title"];
        //update db homepage
        our_works::where('id',$ids)->update([
            // 'image'=>$image,
            'menu'=>$menu_title
        ]);
        $data = our_works::find($ids);
        return $data;
    }
}
