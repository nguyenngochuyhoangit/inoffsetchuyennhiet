<?php

namespace App\Http\Controllers;

use Mail;
use App\Menu;
use App\HomePage;
use Illuminate\Http\Request;
use App\AboutUs;
use App\OutProcess;
use App\our_works;
use App\OurCustomers;
use App\ElementMenuOurWorks;
use App\OurTeam;
use App\Lane1;
use App\Services;

class PageController extends Controller
{
    public function HomePage()
    {
        $data = Menu::all();
        $dataHomePage = HomePage::all();
        $dataAboutUs = AboutUs::all();
        $dataOurProcess = OutProcess::all();
        $dataOUrWorksMenu = our_works::all();
        $dataOurWorks = ElementMenuOurWorks::all();
        $dataOUrCustomers = OurCustomers::all();
        $dataOurTeam = OurTeam::all();
        return view(
            'page.home',
            [
                'data' => $data,
                'dataHomePage' => $dataHomePage,
                'dataAboutUs' => $dataAboutUs,
                'dataOurProcess' => $dataOurProcess,
                'dataOUrWorksMenu' => $dataOUrWorksMenu,
                'dataOurWorks' => $dataOurWorks,
                'dataOUrCustomers' => $dataOUrCustomers,
                'dataOurTeam' => $dataOurTeam,
            ]
        );

    }
    public function AboutUsPage()
    {
        $data1 = Lane1::where('lane', 1)->get();
        $data2 = Lane1::select('logo', 'description', 'alt')->where('lane', 2)->get();
        $data3 = Lane1::where('lane', 3)->get();
        return view(
            'page.about-us',
            [
                'data1' => $data1,
                'data2' => $data2,
                'data3' => $data3,

            ]
        );
    }
    public function ServicesPage()
    {
        $data = Services::all();
        return view('page.services', ['data' => $data]);
    }
    public function ContactMePage()
    {
        return view('page.contact-me');
    }
    public function SendMail(Request $req)
    {
        $data = ['msg' => $req->message, 'name' => $req->name, 'tel' => $req->tel];
        $mail = $req->email;
        $subject = $req->subject;
        Mail::send('email.mail', $data, function ($msg) {
            $msg->from('test.mail.laravel110396@gmail.com');
            $msg->to('test.mail.laravel110396@gmail.com ', 'mail test')->subject('asdsadsa');
        });
    }

}
