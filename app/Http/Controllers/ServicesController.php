<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ServicesController extends Controller
{
    public function showtable()
    {
        $data = Services::all();
        return view('services.services', ['data' => $data]);
    }

    public function insert(Request $request)
    {

        // insert db
        if ($_FILES['image']['name'] == null) {
            $url = null;
        } else {
            $filename = $_FILES['image']['name'];
            Storage::putFileAs('public', new File($_FILES['image']['tmp_name']), $filename);
            $url = Storage::url($filename);
        }
        $insert = new Services;
        $insert->title = "$request->title";
        $insert->subtitle = "$request->subtitle";
        $insert->content_subtitle = "$request->content_subtitle";
        $insert->image = "$url";
        $insert->save();
        return redirect()->back();


    }
    public function delete()
    {
        Services::find($_POST["id"])->delete();
        return;
    }
    public function update()
    {
        //get value form modal
        $ids = $_POST["id"];
        $title = $_POST["title"];
        $content_subtitle = $_POST["content_subtitle"];
        $subtitle = $_POST["subtitle"];
        //update db homepage
        Services::where('id', $ids)->update([
            'title' => $title,
            'content_subtitle' => $content_subtitle,
            // 'image'=>$image,
            'subtitle' => $subtitle
        ]);
        $data = Services::find($ids);
        return $data;
    }
}
