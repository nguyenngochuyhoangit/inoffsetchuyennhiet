<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurCustomers extends Model
{
    protected $table ='ourcustomers';
    protected $fillable = [
        'image','link'
    ];
    public $timestamps = false;
}
