<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{
    protected $table ='homepage';
    protected $fillable = [
        'first_title', 'last_title'
    ];
    public $timestamps = false;
}
