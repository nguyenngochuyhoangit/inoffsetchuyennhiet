<!DOCTYPE html>
<!-- saved from url=(0041)https://gmvdesign.gmvagency.com/services/ -->
<html class="js" lang="vi" itemscope="" itemtype="https://schema.org/WebPage">
<!-- head -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">
    <link rel="apple-touch-icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">
    <!-- wp_head() -->
    <title>Services – GMV DESIGN</title>
    <!-- script | dynamic -->
    <script id="mfn-dnmc-config-js">
        //<![CDATA[
         window.mfn_ajax = "https://gmvdesign.gmvagency.com/wp-admin/admin-ajax.php";
         window.mfn = {
             mobile_init: 1240,
             nicescroll: 40,
             parallax: "translate3d",
             responsive: 1,
             retina_js: 0
         };
         window.mfn_lightbox = {
             disable: false,
             disableMobile: false,
             title: false,
         };
         window.mfn_sliders = {
             blog: 0,
             clients: 0,
             offer: 0,
             portfolio: 0,
             shop: 0,
             slider: 0,
             testimonials: 0
         };
         //]]>
    </script>
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin GMV DESIGN »" href="https://gmvdesign.gmvagency.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi GMV DESIGN »" href="https://gmvdesign.gmvagency.com/comments/feed/">



    <link rel="stylesheet" id="contact-form-7-css" href="{{asset('./statics/css/styles.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset('./statics/css/settings.css')}}" type="text/css" media="all">


    <link rel="stylesheet" id="mfn-base-css" href="{{asset('./statics/css/base.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-layout-css" href="{{asset('./statics/css/layout.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-shortcodes-css" href="{{asset('./statics/css/shortcodes.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-animations-css" href="{{asset('./statics/css/animations.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jquery-ui-css" href="{{asset('./statics/css/jquery.ui.all.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jplayer-css" href="{{asset('./statics/css/jplayer.blue.monday.css')}}" type="text/css" media="all">

    <link rel="stylesheet" id="Roboto-css" href="{{asset('./statics/css/css.css')}}" type="text/css" media="all">

    <link rel="stylesheet" id="style-css" href="{{asset('./statics/css/services.css')}}" type="text/css" media="all">

    <link rel="stylesheet" id="mfn-responsive-css" href="{{asset('./statics/css/responsive.css')}}" type="text/css" media="all">
    <script type="text/javascript" data-cfasync="false" src="{{asset('./statics/js/greensock.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery-migrate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- style | dynamic -->

    <!-- style | custom css | theme options -->

    <!--[if lt IE 9]>
      <script id="mfn-html5" src="https://html5shiv.googlecode.com/svn/trunk/html5.js')}}"></script>
      <![endif]-->
    <!-- script | retina -->

</head>
<!-- body -->

<body class="page-template-default page page-id-3109  color-custom style-default button-stroke layout-full-width if-zoom if-border-hide header-below minimalist-header-no sticky-header sticky-tb-color ab-hide subheader-both-center menu-line-below-80-1 menuo-no-borders logo-no-margin logo-no-sticky-padding footer-copy-center mobile-tb-left mobile-mini-mr-ll be-1783 wpb-js-composer js-comp-ver-5.1.1 vc_responsive nice-scroll">
    <!-- mfn_hook_top -->
    <!-- mfn_hook_top -->
    <!-- #Wrapper -->
    <div id="Wrapper">
        <!-- #Header_bg -->
        <div id="Header_wrapper">
            <!-- #Header -->
            <header id="Header">
                <!-- .header_placeholder 4sticky  -->
                <div class="header_placeholder"></div>
                <div id="Top_bar" class="">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix" style="width: 1080px;">
                                <!-- Logo -->
                                <div class="logo">
                                    <a id="logo" href="https://gmvdesign.gmvagency.com/" title="GMV DESIGN"><img class="logo-main scale-with-grid"
                                            src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}" alt="logo_gmvdesign"><img
                                            class="logo-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign">
                                        <img class="logo-mobile scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"><img class="logo-mobile-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"></a>
                                </div>
                                <div class="menu_wrapper">
                                    <nav id="menu" class="menu-main-menu-left-container">
                                        <ul id="menu-main-menu-left" class="menu">
                                            <li id="menu-item-3108" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="{{url('home_page')}}"><span>Home</span></a></li>
                                            <li id="menu-item-3107" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('about_us_page')}}"><span>About Us</span></a></li>
                                            <li id="menu-item-3114" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3109 current_page_item last"><a href="{{url('services_page')}}"><span>Services</span></a></li>
                                            <li id="menu-item-3117" class="menu-item menu-item-type-post_type menu-item-object-page last"><a href="{{url('contact_me_page')}}"><span>Contact me</span></a></li>
                                        </ul>
                                    </nav>
                                    <a class="responsive-menu-toggle " href="https://gmvdesign.gmvagency.com/services/#"><i
                                            class="icon-menu-fine"></i></a>
                                </div>
                                <div class="secondary_menu_wrapper">
                                    <!-- #secondary-menu -->
                                    <nav id="secondary-menu" class="menu-main-menu-right-container">
                                        <ul id="menu-main-menu-right" class="secondary-menu">
                                            <li id="menu-item-2412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2412"><a href="https://gmvdesign.gmvagency.com/about-2/">About</a></li>
                                            <li id="menu-item-2411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2411"><a href="https://gmvdesign.gmvagency.com/contact-me/">Contact me</a></li>
                                            <li id="menu-item-2391" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2391"><a target="_blank" href="http://themeforest.net/item/betheme-responsive-multipurpose-wordpress-theme/7758048?ref=muffingroup">Buy
                                                    now</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="banner_wrapper">
                                </div>
                                <div class="search_wrapper">
                                    <!-- #searchform -->
                                    <form method="get" id="searchform" action="https://gmvdesign.gmvagency.com/">
                                        <i class="icon_search icon-search-fine"></i>
                                        <a href="https://gmvdesign.gmvagency.com/services/#" class="icon_close"><i
                                                class="icon-cancel-fine"></i></a>
                                        <input type="text" class="field" name="s" id="s" placeholder="Enter your search">
                                        <input type="submit" class="submit" value="" style="display:none;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- mfn_hook_content_before -->
        <!-- mfn_hook_content_before -->
        <!-- #Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <!-- .sections_group -->
                <div class="sections_group">
                    <div class="entry-content" itemprop="mainContentOfPage">
                        <div class="section mcb-section" style="padding-top:0px; padding-bottom:0px; background-color:">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one-second  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_image ">
                                            <div class="image_frame image_item no_link scale-with-grid aligncenter stretch no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="{{asset($data[0]->image)}}" alt="services-us"
                                                        width="608" height="647"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrap mcb-wrap one-second  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_accordion tabs-services">
                                            <div class="accordion">
                                                <h4 class="title"><span style="padding:30px 15px; background-color:#ef6124;font-weight:bold;color:#fff;margin:15px;">{{$data[0]->title}}</span></h4>
                                                <div class="mfn-acc accordion_wrapper  open1st">
                                                    @for ($i = 0; $i
                                                    < count($data); $i++) <div class="question">
                                                        <div class="title"><i class="icon-plus acc-icon-plus"></i><i class="icon-minus acc-icon-minus"></i><b>{{$data[$i]->subtitle}}</b>
                                                        </div>
                                                        <div class="answer ">
                                                            <p style="color:#2a2a2a; ">
                                                                {{$data[0]->content_subtitle}}
                                                            </p>
                                                        </div>
                                                </div>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section mcb-section " style="padding-top:0px; padding-bottom:0px; background-color:#2b2c30 ">
                        <div class="section_wrapper mcb-section-inner ">
                            <div class="wrap mcb-wrap one valign-top clearfix " style=" ">
                                <div class="mcb-wrap-inner ">
                                    <div class="column mcb-column one column_image ">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter stretch
                                                            no_border ">
                                            <div class="image_wrapper "><img class="scale-with-grid " src="{{asset(
                                                            './statics/images/works-gmvdesign.png')}} " alt="works-gmvdesign "
                                                    width="1104 " height="857 "></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section the_content no_content ">
                        <div class="section_wrapper ">
                            <div class="the_content_wrapper "></div>
                        </div>
                    </div>
                    <div class="section section-page-footer ">
                        <div class="section_wrapper clearfix ">
                            <div class="column one page-pager ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .four-columns - sidebar -->
        </div>
    </div>
    <!-- mfn_hook_content_after -->
    <!-- mfn_hook_content_after -->
    <!-- #Footer -->
    <footer id="Footer " class="clearfix ">
        <div class="widgets_wrapper " style="padding:50px 0 35px; ">
            <div class="container ">
                <div class="column one-third ">
                    <aside id="text-3 " class="widget widget_text ">
                        <h4>GMV DESIGN</h4>
                        <div class="textwidget ">
                            <ul style="line-height: 32px; ">
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Tax
                                            : 0 3 1 4 5 0 4 1 4 2</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Cell:
                                            0163 977 7548</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Email
                                            : cherrytien@gmvgroup.vn</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Address
                                            : 62 Nguyen Quang Bich str , ward 13 , Tan Binh District , HCMC , Vietnam.
                                        </a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="column one-third ">
                    <aside id="text-4 " class="widget widget_text ">
                        <h4>SERVICES</h4>
                        <div class="textwidget ">
                            <ul style="line-height: 32px; ">
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Branding
                                            Design</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Creative
                                            Design</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Digital
                                            Design</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">Storyboard
                                            Clip </a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="column one-third ">
                    <aside id="text-5 " class="widget widget_text ">
                        <h4>POLICY</h4>
                        <div class="textwidget ">
                            <ul style="line-height: 32px; ">
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">GMV
                                            Client </a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">GMV
                                            Partnership</a></li>
                                <li><i class="icon-right-dir " style="color: #77acfb; "></i> <a href="https://gmvdesign.gmvagency.com/services/# ">GMV
                                            Business Group</a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <div class="footer_copy ">
            <div class="container ">
                <div class="column one ">
                    <!-- Copyrights -->
                    <div class="copyright ">
                        © 2018 GMV DESIGN. All Rights Reserved. <a target="_blank " rel="nofollow " href="http://muffingroup.com/ ">Muffin
                                group</a>
                    </div>
                    <ul class="social "></ul>
                </div>
            </div>
        </div>
    </footer>
    </div>
    <!-- #Wrapper -->
    <!-- mfn_hook_bottom -->
    <!-- mfn_hook_bottom -->
    <!-- wp_footer() -->
    <script type="text/javascript ">
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings ": {
                "root ": "https:\/\/gmvdesign.gmvagency.com\/wp-json\/contact-form-7\/v1 ",
                "namespace ": "contact-form-7\/v1 "
            },
            "recaptcha ": {
                "messages ": {
                    "empty ": "H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot. "
                }
            }
        };
        /* ]]> */
    </script>
    <script type="text/javascript " src="{{asset( './statics/js/services.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/core.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/widget.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/mouse.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/sortable.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/tabs.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/accordion.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/plugins.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/menu.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/animations.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/jplayer.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/translate3d.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/scripts(1).js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/comment-reply.min.js')}} "></script>
    <script type="text/javascript " src="{{asset( './statics/js/wp-embed.min.js')}} "></script>

</body>

</html>