<!DOCTYPE html>
<!-- saved from url=(0043)https://gmvdesign.gmvagency.com/contact-me/ -->
<html class="js" lang="vi" itemscope="" itemtype="https://schema.org/WebPage">
<!-- head -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">
    <link rel="apple-touch-icon" href="https://gmvdesign.gmvagency.com/wp-content/uploads/2017/10/gmv-logo.png')}}">
    <!-- wp_head() -->
    <title>Contact me – </title>
    <!-- script | dynamic -->
    <script id="mfn-dnmc-config-js">
        //<![CDATA[
        window.mfn_ajax ="https://gmvdesign.gmvagency.com/wp-admin/admin-ajax.php";
        window.mfn = {
            mobile_init: 1240,
            nicescroll: 40,
            parallax:"translate3d",
            responsive: 1,
            retina_js: 0
        };
        window.mfn_lightbox = {
            disable: false,
            disableMobile: false,
            title: false,
        };
        window.mfn_sliders = {
            blog: 0,
            clients: 0,
            offer: 0,
            portfolio: 0,
            shop: 0,
            slider: 0,
            testimonials: 0
        };
        //]]>
    </script>
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin HOME DESIGN »" href="#">
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi HOME DESIGN »" href="#">

    <script src="{{asset('./statics/js/wp-emoji-release.min.js')}}" type="text/javascript" defer=""></script>

    <link rel="stylesheet" id="layerslider-css" href="{{asset('./statics/css/layerslider.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="{{asset('./statics/css/styles.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="{{asset('./statics/css/settings.css')}}" type="text/css" media="all">


    <link rel="stylesheet" id="mfn-base-css" href="{{asset('./statics/css/base.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-layout-css" href="{{asset('./statics/css/layout.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-shortcodes-css" href="{{asset('./statics/css/shortcodes.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-animations-css" href="{{asset('./statics/css/animations.min.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jquery-ui-css" href="{{asset('./statics/css/jquery.ui.all.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-jplayer-css" href="{{asset('./statics/css/jplayer.blue.monday.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="style-css" href="{{asset('./statics/css/contact-me.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="mfn-responsive-css" href="{{asset('./statics/css/responsive.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="Roboto-css" href="{{asset('./statics/css/css.css')}}" type="text/css" media="all">

    <script type="text/javascript" data-cfasync="false" src="{{asset('./statics/js/greensock.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery-migrate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jquery.themepunch.revolution.min.js')}}"></script>

</head>
<!-- body -->

<body class="page-template-default page page-id-15  color-custom style-default button-stroke layout-full-width if-zoom if-border-hide no-content-padding header-below minimalist-header-no sticky-header sticky-tb-color ab-hide subheader-both-center menu-line-below-80-1 menuo-no-borders logo-no-margin logo-no-sticky-padding footer-copy-center mobile-tb-left mobile-mini-mr-ll be-1783 wpb-js-composer js-comp-ver-5.1.1 vc_responsive nice-scroll">
    <!-- mfn_hook_top -->
    <!-- mfn_hook_top -->
    <!-- #Wrapper -->
    <div id="Wrapper">
        <!-- #Header_bg -->
        <div id="Header_wrapper">
            <!-- #Header -->
            <header id="Header">
                <!-- .header_placeholder 4sticky  -->
                <div class="header_placeholder"></div>
                <div id="Top_bar" class="">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix" style="width: 1080px;">
                                <!-- Logo -->
                                <div class="logo">
                                    <a id="logo" href="https://gmvdesign.gmvagency.com/" title="GMV DESIGN"><img class="logo-main scale-with-grid"
                                            src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}" alt="logo_gmvdesign"><img
                                            class="logo-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign">
                                        <img class="logo-mobile scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"><img class="logo-mobile-sticky scale-with-grid" src="{{asset('./statics/images/logo/logo_gmvdesign.png')}}"
                                            alt="logo_gmvdesign"></a>
                                </div>
                                <div class="menu_wrapper">
                                    <nav id="menu" class="menu-main-menu-left-container">
                                        <ul id="menu-main-menu-left" class="menu">
                                            <li id="menu-item-3108" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="{{url('home_page')}}"><span>Home</span></a></li>
                                            <li id="menu-item-3107" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('about_us_page')}}"><span>About Us</span></a></li>
                                            <li id="menu-item-3114" class="menu-item menu-item-type-post_type menu-item-object-page last"><a href="{{url('services_page')}}"><span>Services</span></a></li>
                                            <li id="menu-item-3117" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-15 current_page_item last"><a href="{{url('contact_me_page')}}"><span>Contact me</span></a></li>
                                        </ul>
                                    </nav>
                                    <a class="responsive-menu-toggle" href="https://gmvdesign.gmvagency.com/contact-me/#"><i
                                            class="icon-menu-fine"></i></a>
                                </div>
                                <div class="secondary_menu_wrapper">
                                    <!-- #secondary-menu -->
                                    <nav id="secondary-menu" class="menu-main-menu-right-container">
                                        <ul id="menu-main-menu-right" class="secondary-menu">
                                            <li id="menu-item-2412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2412"><a href="https://gmvdesign.gmvagency.com/about-2/">About</a></li>
                                            <li id="menu-item-2411" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-15 current_page_item menu-item-2411"><a href="https://gmvdesign.gmvagency.com/contact-me/">Contact me</a></li>
                                            <li id="menu-item-2391" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2391"><a target="_blank" href="http://themeforest.net/item/betheme-responsive-multipurpose-wordpress-theme/7758048?ref=muffingroup">Buy
                                                    now</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="banner_wrapper">
                                </div>
                                <div class="search_wrapper">
                                    <!-- #searchform -->
                                    <form method="get" id="searchform" action="https://gmvdesign.gmvagency.com/">
                                        <i class="icon_search icon-search-fine"></i>
                                        <a href="https://gmvdesign.gmvagency.com/contact-me/#" class="icon_close"><i
                                                class="icon-cancel-fine"></i></a>
                                        <input type="text" class="field" name="s" id="s" placeholder="Enter your search">
                                        <input type="submit" class="submit" value="" style="display:none;">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- mfn_hook_content_before -->
        <!-- mfn_hook_content_before -->
        <!-- #Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <!-- .sections_group -->
                <div class="sections_group">
                    <div class="entry-content" itemprop="mainContentOfPage">
                        <div class="section mcb-section" style="padding-top:140px; padding-bottom:90px; background-color:#141b21; background-image:url(https://gmvdesign.gmvagency.com/wp-content/uploads/2016/10/home_snapshot_contact1.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:; background-size:; -webkit-background-size:">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one-sixth  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_placeholder">
                                            <div class="placeholder">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrap mcb-wrap two-third  valign-top clearfix" style="">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column  column-margin-">
                                            <div class="column_attr clearfix align_center" style="">
                                                <h1 style="color: #fff;">CONTACT US</h1>
                                                <hr class="no_line" style="margin: 0 auto 20px;">
                                                <h4 style="color: #fff;">
                                                </h4>
                                                <hr class="no_line" style="margin: 0 auto 20px;">
                                                <div role="form" class="wpcf7" id="wpcf7-f2446-p15-o1" lang="en-US" dir="ltr">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="{{route('sendmail')}}" method="post" class="wpcf7-form" novalidate="novalidate">
                                                        @csrf
                                                        <div style="display: none;">
                                                            <input type="hidden" name="_wpcf7" value="2446">
                                                            <input type="hidden" name="_wpcf7_version" value="4.9.1">
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2446-p15-o1">
                                                            <input type="hidden" name="_wpcf7_container_post" value="15">
                                                        </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-subject">
                                                              <input
                                                                 type="text" name="subject" value="" size="40"
                                                                 class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                 aria-required="true" aria-invalid="false"
                                                                 placeholder="Title">
                                                              </span>
                                                        </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-name">
                                                              <input
                                                                 type="text" name="name" value="" size="40"
                                                                 class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                 aria-required="true" aria-invalid="false"
                                                                 placeholder="Full Name">
                                                              </span>
                                                        </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap your-email">
                                                              <input
                                                                 type="email" name="email" value="" size="40"
                                                                 class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                 aria-required="true" aria-invalid="false"
                                                                 placeholder="Email">
                                                              </span>
                                                        </div>
                                                        <div class="column one-second"><span class="wpcf7-form-control-wrap tel-684">
                                                              <input
                                                                 type="tel" name="tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                 aria-required="true" aria-invalid="false"
                                                                 placeholder="Phone">
                                                              </span>
                                                        </div>
                                                        <div class="column one"><span class="wpcf7-form-control-wrap your-message">
                                                              <textarea
                                                                 name="message" cols="40" rows="8" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                 aria-required="true" aria-invalid="false"
                                                                 placeholder="Message to GMV Design..."></textarea>
                                                              </span>
                                                        </div>
                                                        <div class="column one">
                                                            <input type="submit" value="SEND" class="wpcf7-form-control wpcf7-submit button_full_width">
                                                            <span class="ajax-loader">
                                                                </span>
                                                        </div>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section the_content no_content">
                            <div class="section_wrapper">
                                <div class="the_content_wrapper"></div>
                            </div>
                        </div>
                        <div class="section section-page-footer">
                            <div class="section_wrapper clearfix">
                                <div class="column one page-pager">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .four-columns - sidebar -->
            </div>
        </div>
        <!-- mfn_hook_content_after -->
        <!-- mfn_hook_content_after -->
        <!-- #Footer -->
        <footer id="Footer" class="clearfix">
            <div class="widgets_wrapper" style="padding:50px 0 35px;">
                <div class="container">
                    <div class="column one-third">
                        <aside id="text-3" class="widget widget_text">
                            <h4>GMV DESIGN</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Tax
                                            : 0 3 1 4 5 0 4 1 4 2</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Cell:
                                            0163 977 7548</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Email
                                            : cherrytien@gmvgroup.vn</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Address
                                            : 62 Nguyen Quang Bich str , ward 13 , Tan Binh District , HCMC , Vietnam.
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="column one-third">
                        <aside id="text-4" class="widget widget_text">
                            <h4>SERVICES</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Branding
                                            Design</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Creative
                                            Design</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Digital
                                            Design</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">Storyboard
                                            Clip </a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="column one-third">
                        <aside id="text-5" class="widget widget_text">
                            <h4>POLICY</h4>
                            <div class="textwidget">
                                <ul style="line-height: 32px;">
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">GMV
                                            Client </a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">GMV
                                            Partnership</a>
                                    </li>
                                    <li><i class="icon-right-dir" style="color: #77acfb;"></i> <a href="https://gmvdesign.gmvagency.com/contact-me/#">GMV
                                            Business Group</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">
                        <!-- Copyrights -->
                        <div class="copyright">
                            © 2018 GMV DESIGN. All Rights Reserved. <a target="_blank" rel="nofollow" href="http://muffingroup.com/">Muffin
                                group</a>
                        </div>
                        <ul class="social"></ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- #Wrapper -->
    <!-- mfn_hook_bottom -->
    <!-- mfn_hook_bottom -->
    <!-- wp_footer() -->

    <script type="text/javascript" src="{{asset('./statics/js/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/core.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/widget.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/mouse.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/sortable.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/tabs.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/accordion.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/menu.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/animations.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/jplayer.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/translate3d.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/scripts(1).js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/comment-reply.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('./statics/js/wp-embed.min.js')}}"></script>
    <div id="ascrail2000" class="nicescroll-rails nicescroll-rails-vr" style="width: 10px; z-index: auto; cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; opacity: 1; display: block;">
        <div class="nicescroll-cursors" style="position: relative; top: 0px; float: right; width: 10px; height: 258px; background-color: rgb(34, 34, 34); border: 0px; background-clip: padding-box; border-radius: 5px;"></div>
    </div>
</body>

</html>