@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
        <!-- Horizontal Form -->
        <div class="card">
          <div class="card-header">
            <h3 class="box-title"> Register</h3>
           
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="card-body">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                    <strong>Lỗi</strong>
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                          @endforeach
                      </ul>
                </div>
                @endif
            <form class="form-horizontal" method="POST" action="{{route('Register')}}">
                @csrf
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control"  name="name" placeholder="Name">
                        </div>
                      </div>
                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control"  name="email" placeholder="Emai">
                        </div>
                      </div>
                  <div class="form-group">
                    <label  class="col-sm-2 control-label">UserName</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name="username" placeholder="User Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          {{-- <button type="submit" class="btn btn-default">Cancel</button> --}}
                          <button type="submit" class="btn btn-info pull-right">Register</button>
                        </label>
                      </div>
                    </div>
                  </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
            </form>
          </div>
        </div>
    </div>
  </div>
</div>  
@endsection